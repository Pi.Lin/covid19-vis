# Installation (for Debian or Ubuntu)

```
git clone https://gitlab.com/Pi.Lin/covid19-vis
cd covid19-vis
pipenv install
```

# Usage

![Screenshot_from_2023-01-10_19-20-05](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/0683e1a34d68d7426d9b4c618e156001/Screenshot_from_2023-01-10_19-20-05.png)

# Visuals

![Screenshot_from_2023-01-10_19-25-32](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/2244da01f5f1026c818604789ea8b7d4/Screenshot_from_2023-01-10_19-25-32.png)

![Screenshot_from_2023-01-10_19-25-44](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/eec35e92ed4b8631d2d72d623e52c1ca/Screenshot_from_2023-01-10_19-25-44.png)


![Screenshot_from_2023-01-18_12-19-20](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/86d9a42bbda6fc7acf980a65dc983ba2/Screenshot_from_2023-01-18_12-19-20.png)

![Screenshot_from_2023-01-18_12-19-04](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/d76193036de4cd623cfc23f56b6721bb/Screenshot_from_2023-01-18_12-19-04.png)


![Screenshot_from_2023-01-18_19-12-14](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/044ab3a6ee687fbbc54f563fee44583c/Screenshot_from_2023-01-18_19-12-14.png)

![Screenshot_from_2023-01-18_19-11-48](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/94c91daf31d8364e250a7ae7efb5db10/Screenshot_from_2023-01-18_19-11-48.png)

![Screenshot_from_2023-01-18_19-09-08](https://gitlab.com/Pi.Lin/covid19-vis/-/wikis/uploads/9cf13b6f1fbe6670b97476cc2803f2d2/Screenshot_from_2023-01-18_19-09-08.png)


## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

- [Peter Lin](mailto:lin.pifeng@gmail.com)

## Data source

- [Epidemiological Data from the COVID-19 Epidemic in Canada](https://github.com/ccodwg/Covid19Canada)


## PythonAnywhere Deploymment 

https://plin.pythonanywhere.com/

## References

- [Pygal Documentation](https://www.pygal.org/en/stable/)

